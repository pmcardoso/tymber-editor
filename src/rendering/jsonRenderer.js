import React from 'react'

function render({types, jsonPage}) {
    return renderComponent({uuid: jsonPage.root, types, jsonPage})
}

function renderComponent({uuid, types, jsonPage}) {
    const component = jsonPage.components[uuid];

    const Component = types[component.type];

    const children = component.props.children || [];

    return (
        <Component key={uuid} {...component.props} uuid={uuid}>
            {children.map((uuid) => renderComponent({ uuid, types, jsonPage }))}
        </Component>
    )
}

export function renderOptions({types, options}) {
    return (
        <>
            {options.fields.map((option, index) => {
                const {type, props} = option;

                const Component = types[type];

                return <Component key={index} {...props} optionsFor={options.for} />
            })}
        </>
    )
}

export default render
