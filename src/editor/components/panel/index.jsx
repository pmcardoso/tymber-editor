import React from 'react'
import styled from 'styled-components'
import PageBrowser from "../PageBrowser";
import CoreComponentsConfig from "../../../core-components";
import {renderOptions} from "../../../rendering/jsonRenderer";
import useEditOptions from "../../hooks/useEditOptions";

const EditorPanelContainer = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    width: 350px;
    height: 100%;
    background-color: white;
    box-shadow: 4px 0px 102px -56px rgba(0,0,0,0.75);
    padding: 15px;
    box-sizing: border-box;
`;

const ComponentEditor = styled.div`
    margin-top: 32px;
    h2 {
        font-size: 16px;
    }
`;

function Index() {

    const [options,] = useEditOptions();
    const types = CoreComponentsConfig.editorComponents.types;

    return (
        <EditorPanelContainer>
            <PageBrowser/>
            {options && <ComponentEditor>
                <h2>Component Options</h2>
                <hr/>
                {renderOptions({types, options})}
            </ComponentEditor>}
        </EditorPanelContainer>
    )
}

export default Index
