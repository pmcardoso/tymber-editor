import React from 'react'
import render from "../../rendering/jsonRenderer";
import usePage from "../hooks/usePage";
import CoreComponentsConfig from '../../core-components';
import styled from 'styled-components';

const Container = styled.div`
    margin-left: 350px;
`;

function Page() {
    const [page,] = usePage();

    const types = CoreComponentsConfig.components.types;

    return (
        <Container>
            {page && render({types, jsonPage: page})}
        </Container>
    )
}

export default Page
