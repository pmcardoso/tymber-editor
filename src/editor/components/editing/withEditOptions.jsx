import React from 'react'
import styled from 'styled-components'
import useEditOptions from "../../hooks/useEditOptions";

const Container = styled.div`
    &:hover {
        border: 1px solid #872617;
    }
    cursor: pointer;
`;

function withEditOptions(options, Component) {
    return (props) => {

        const [, setEditOptions] = useEditOptions();

        const {uuid} = props;

        const handleSelect = (evt) => {
            setEditOptions({...options, for: uuid});
            evt.stopPropagation()
        };

        return (
            <Container onClick={handleSelect}>
                <Component {...props}/>
            </Container>
        )
    }
}

export default withEditOptions
