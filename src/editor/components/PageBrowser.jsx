import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import usePage from "../hooks/usePage";
import useSite from "../hooks/useSite";

const useStyles = makeStyles(theme => ({
    formControl: {
        minWidth: '100%',
        boxSizing: 'border-box'
    }
}));

function PageBrowser() {
    const classes = useStyles();
    const [page, setPage] = usePage();
    const [site,] = useSite();

    const pages = site.pages;
    const handleChange = event => {
        setPage(event.target.value);
    };

    return (
        <FormControl className={classes.formControl}>
            <InputLabel id="page-browser-label">Page</InputLabel>
            <Select
                labelId="page-browser-label"
                id="page-browser-select"
                value={page}
                onChange={handleChange}
            >
                {pages.map((page, index) => <MenuItem value={page} key={index}>{page.name}</MenuItem>)}
            </Select>
        </FormControl>
    )
}

export default PageBrowser
