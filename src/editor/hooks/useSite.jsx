import {useDispatch, useSelector} from "react-redux";
import {getSite} from "../redux/selectors";
import {setSite} from "../redux/actions";

function useSite() {
    const dispatch = useDispatch();
    const page = useSelector(getSite);

    const selectSite = (site) => {
        dispatch(setSite(site))
    };

    return [page, selectSite]
}

export default useSite
