import {useDispatch, useSelector} from "react-redux";
import {getPage} from "../redux/selectors";
import {setPage} from "../redux/actions";

function usePage() {
    const dispatch = useDispatch();
    const page = useSelector(getPage);

    const selectPage = (page) => {
        dispatch(setPage(page))
    };

    return [page, selectPage]
}

export default usePage
