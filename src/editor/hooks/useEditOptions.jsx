import {useDispatch, useSelector} from "react-redux";
import {getEditOptions} from "../redux/selectors";
import {setEditOptions} from "../redux/actions";

function useEditOptions() {
    const dispatch = useDispatch();
    const editOptions = useSelector(getEditOptions);

    const selectEditOptions = (options) => {
        dispatch(setEditOptions(options))
    };

    return [editOptions, selectEditOptions]
}

export default useEditOptions
