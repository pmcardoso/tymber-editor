import uuid from "uuid/v4";

export function addChild({page, to, type, props = {}}) {
    const newComponentUUID = uuid();
    return {
        ...page,
        components: {
            ...page.components,
            [to]: {
                ...page.components[to],
                props: {
                    ...page.components[to].props,
                    children: [
                        ...page.components[to].props.children,
                        newComponentUUID
                    ]
                }
            },
            [newComponentUUID]: {
                type: type,
                props: {
                    children: [],
                    ...props
                }
            }
        }
    };
}

export function changeProperty({page, uuid, field, value}) {
    return {
        ...page,
        components: {
            ...page.components,
            [uuid]: {
                ...page.components[uuid],
                props: {
                    ...page.components[uuid].props,
                    [field]: value
                }
            }
        }
    };
}

export function getProperty({page, uuid, field}) {
    return page.components[uuid].props[field]
}
