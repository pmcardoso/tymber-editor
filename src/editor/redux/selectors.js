export function getPage(state) {
    return state.page
}

export function getSite(state) {
    return state.site
}

export function getEditOptions(state) {
    return state.editOptions
}