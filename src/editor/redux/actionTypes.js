export default {
    SET_PAGE: 'editor/set-page',
    SET_SITE: 'editor/set-site',
    SET_EDIT_OPTIONS: 'editor/set-edit-options'
}
