import {combineReducers} from "redux";
import ActionTypes from './actionTypes';

export function page(state = null, action) {
    switch(action.type) {
        case ActionTypes.SET_PAGE:
            return action.payload.page;
        default:
            return state
    }
}


const initialSite = {
    pages: [
        {
            path: '/',
            name: "Home",
            root: "uuid-1",
            components: {
                "uuid-1": {
                    type: "vertical-layout",
                    props: {
                        children: ["uuid-2"],
                    }
                },
                "uuid-2": {
                    type: "text",
                    props: {
                        text: ""
                    }
                }
            }
        },
        {
            path: '/products/:id',
            name: "Product Detail",
            root: "uuid-1",
            components: {
                "uuid-1": {
                    type: "vertical-layout",
                    props: {
                        children: []
                    }
                }
            }
        }
    ]
};

export function site(state = initialSite, action) {
    switch(action.type) {
        case ActionTypes.SET_SITE:
            return action.payload.site;
        default:
            return state
    }
}

export function editOptions(state = null, action) {
    switch(action.type) {
        case ActionTypes.SET_EDIT_OPTIONS:
            return action.payload.options;
        default:
            return state
    }
}

export default combineReducers({
    page,
    site,
    editOptions
})
