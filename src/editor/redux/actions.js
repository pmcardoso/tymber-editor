import ActionTypes from './actionTypes';

export function setPage(page) {
    return {
        type: ActionTypes.SET_PAGE,
        payload: { page }
    }
}

export function setSite(site) {
    return {
        type: ActionTypes.SET_SITE,
        payload: { site }
    }
}

export function setEditOptions(options) {
    return {
        type: ActionTypes.SET_EDIT_OPTIONS,
        payload: { options }
    }
}
