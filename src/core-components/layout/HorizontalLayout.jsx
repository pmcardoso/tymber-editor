import styled from 'styled-components'
import makeLayout from "./layoutFactory";
import withEditOptions from "../../editor/components/editing/withEditOptions";

const Container = styled.div`
    display: flex;
    flex-flow: wrap;
    height: 100px;
`;

const HorizontalLayout = withEditOptions({
    fields: [{
        type: 'children-editor',
        props: {
            label: 'Content',
            field: 'children'
        }
    }]
}, makeLayout(Container));

HorizontalLayout.editorName = 'Horizontal Layout';

export default HorizontalLayout
