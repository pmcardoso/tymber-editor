import React from 'react'

function makeLayout(LayoutComponent) {
    function Layout({children, className, style}) {
        return (
            <LayoutComponent style={style} className={className}>
                {children}
            </LayoutComponent>
        )
    }

    return Layout
}

export default makeLayout
