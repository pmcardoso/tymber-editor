import styled from 'styled-components'
import makeLayout from "./layoutFactory";
import withEditOptions from "../../editor/components/editing/withEditOptions";

const Container = styled.div`
    display: flex;
    flex-direction: column;
    height: 100px;
`;

const VerticalLayout = withEditOptions({
    fields: [{
        type: 'children-editor',
        props: {
            label: 'Content',
            field: 'children'
        }
    }]
}, makeLayout(Container));

VerticalLayout.editorName = 'Vertical Layout';

export default VerticalLayout
