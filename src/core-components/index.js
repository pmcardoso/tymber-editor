import VerticalLayout from "./layout/VerticalLayout";
import HorizontalLayout from "./layout/HorizontalLayout";
import ChildrenEditor from "./editing/ChildrenEditor";
import TextEditor from "./editing/TextEditor";
import Text from './general/Text'

export default {
    components: {
        types: {
            'vertical-layout': VerticalLayout,
            'horizontal-layout': HorizontalLayout,
            'text': Text
        }
    },
    editorComponents: {
        types: {
            'children-editor': ChildrenEditor,
            'text-editor': TextEditor
        }
    }
}
