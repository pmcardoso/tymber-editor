import React from 'react'
import withEditOptions from "../../editor/components/editing/withEditOptions";

function Text({text}) {
    return (
        <div dangerouslySetInnerHTML={{__html: text || "<p>Placeholder Text</p>"}}/>
    )
}

export default withEditOptions({
    fields: [{
        type: 'text-editor',
        props: {
            label: 'Title',
            field: 'text'
        }
    }]
}, Text);


