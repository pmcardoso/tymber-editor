import React from 'react';
import {Editor, EditorState, ContentState, RichUtils, convertFromHTML} from 'draft-js';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';
import styled from 'styled-components'
import usePage from "../../editor/hooks/usePage";
import {changeProperty, getProperty} from "../../editor/templateEditor";
import {stateToHTML} from 'draft-js-export-html';


function TextEditor({label, optionsFor, field}) {
    const [page, setPage] = usePage();

    const initialValue = getProperty({page, uuid: optionsFor, field}) || "<p>Placeholder Text</p>";

    const blocksFromHTML = convertFromHTML(initialValue);
    const state = ContentState.createFromBlockArray(
        blocksFromHTML.contentBlocks,
        blocksFromHTML.entityMap
    );

    const [editorState, setEditorState] = React.useState(
        EditorState.createWithContent(state)
    );

    const editor = React.useRef(null);

    function focusEditor() {
        editor.current.focus();
    }

    React.useEffect(() => {
        focusEditor()
    }, []);

    const handleKeyCommand = (command, editorState) => {
        const newState = RichUtils.handleKeyCommand(editorState, command);
        if (newState) {
            setEditorState(newState);
            return 'handled';
        }
        return 'not-handled';
    };

    const handleChange = (editorState) => {
        const html = stateToHTML(editorState.getCurrentContent());
        const newPage = changeProperty({page, uuid: optionsFor, field, value: html});
        setPage(newPage);
        setEditorState(editorState);
    };

    return (
        <div>
            <Label>{label}</Label>
            <Controls>
                <ButtonGroup size="small" aria-label="small outlined button group">
                    <Button>B</Button>
                    <Button>I</Button>
                    <Button>U</Button>
                </ButtonGroup>
            </Controls>

            <EditorContainer onClick={focusEditor}>
                <Editor
                    ref={editor}
                    editorState={editorState}
                    handleKeyCommand={handleKeyCommand}
                    onChange={handleChange}
                />
            </EditorContainer>
        </div>
    );
}

const Label = styled.label`
    font-weight: 500;
`;

const Controls = styled.div`
    margin-top: 10px;
    margin-bottom: 10px;
`;

const EditorContainer = styled.div`
    border: 1px solid #eee;
    border-radius: 2px;
    padding: 10px;
    min-height: 150px;
`;


export default TextEditor
