import React, {useState} from 'react'
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import CoreComponentsConfig from '../../core-components';
import usePage from "../../editor/hooks/usePage";
import {addChild} from "../../editor/templateEditor";

const useStyles = makeStyles(theme => ({
    formControl: {
        minWidth: '100%',
        boxSizing: 'border-box'
    }
}));

function ChildrenEditor({label, optionsFor}) {

    const classes = useStyles();

    const [selectedComponent, setComponent] = useState(undefined);

    const [page, setPage] = usePage();

    const handleChange = event => {
        setComponent(event.target.value);
    };

    const types = Object.keys(CoreComponentsConfig.components.types).map((key) => ({
        type: key,
        component: CoreComponentsConfig.components.types[key]
    }));

    const handleAdd = () => {
        if (selectedComponent) {
            const newPage = addChild({page, to: optionsFor, type: selectedComponent});
            setPage(newPage)
        }
    };

    return (
        <FormControl className={classes.formControl}>
            <InputLabel id="children-editor-label">Children</InputLabel>
            <Select
                labelId="children-editor-label"
                id="children-editor-label"
                value={selectedComponent}
                onChange={handleChange}
            >
                {types.map((type, index) => <MenuItem value={type.type} key={index}>{type.component.editorName}</MenuItem>)}
            </Select>
            <Button color="primary" onClick={handleAdd}>Add</Button>
        </FormControl>
    )
}

export default ChildrenEditor
