import React from 'react';
import './App.css';
import Index from "./editor/components/panel";
import {Provider} from "react-redux";
import store from "./editor/redux/store";
import Page from "./editor/components/Page";

function App() {
  return (
      <Provider store={store}>
        <Index/>
        <Page/>
      </Provider>
  );
}

export default App;
